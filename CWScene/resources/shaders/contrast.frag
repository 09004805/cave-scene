#version 440
// Sampler used to get texture colour
uniform sampler2D frame_buffer;

// Takes incoming contrast value
uniform float contrast_value;

// Incoming texture coordinate
layout (location = 0) in vec2 tex_coord;

// Outgoing colour
layout (location = 0) out vec4 out_colour;


void main()
{

	vec4 tex_colour = texture(frame_buffer, tex_coord);


	tex_colour -= 0.5;
	tex_colour *= (1.0 + contrast_value);
	tex_colour += 0.5;

	tex_colour.a = 1.0;

	tex_colour = out_colour;

}