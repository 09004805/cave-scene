#version 440
// Sampler used to get texture colour
uniform sampler2D tex;
// Incoming texture coordinate
layout (location = 0) in vec2 tex_coord;
// Outgoing colour
layout (location = 0) out vec4 out_colour;
void main()
{
	// ****************************************
	// Set out colour to sampled texture colour
	// ****************************************
	vec4 tex_colour = texture(tex, tex_coord);
	float greyscale = (tex_colour.r * 0.299) + (tex_colour.g *0.587) + (tex_colour.b * 0.184);
	out_colour = vec4(greyscale, greyscale, greyscale, 1);
}