#version 440

// Incoming normal for diffuse 
layout (location = 2) in vec3 normal;

// light direction
uniform vec3 lightDir;


// The transformation matrix
uniform mat4 MVP;

// The ambient intensity of the scene
uniform float ambient_intensity;

// The material colour of the object
uniform vec4 material_colour;

// Combine texture.vert into this shader file
layout (location = 10) in vec2 tex_coord_in;

layout (location = 1) out vec2 tex_coord_out;

// Incoming position data
layout (location = 0) in vec3 position;

// The outgoing vertex colour
layout (location = 4) out vec4 vertex_colour;

void main()
{
	
	// ******************
	// Calculate position
	// ******************
	gl_Position = MVP * vec4(position, 1.0);

	// Calculate diffuse colour
	vec4 diffCol = material_colour * max(dot(normalize(normal), lightDir), 0);


	// ***************************
	// Calculate & output ambient component
	// ***************************

	vertex_colour = ambient_intensity * material_colour + diffCol;

	
	tex_coord_out = tex_coord_in;

}