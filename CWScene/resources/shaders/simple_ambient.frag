#version 440
// Sampler used to get texture colour
uniform sampler2D tex;




// Incoming texture coordinate
layout (location = 1) in vec2 tex_coord;

// Incoming vertex colour
layout (location = 4) in vec4 vertex_colour;

// Outgoing colour
layout (location = 0) out vec4 colour;

void main()
{
 
	// **************************
	// Set outgoing vertex colour
	// **************************
	vec4 col = texture(tex, tex_coord);
	colour = col * vertex_colour;
	colour.a = col.a;
}