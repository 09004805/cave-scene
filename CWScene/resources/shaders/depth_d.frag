#version 440

// Incoming frame data
uniform sampler2D tex;

// Depth tex coming from main
uniform sampler2D depthTex;

// 1.0f / screen width
uniform float invWidth;
// 1.0f / screen height
uniform float invHeight;

// Surrounding pixels to sample and their scale
const vec4 samples[8] = vec4[8]
(
    vec4(0.0, 0.0, 0.0, 0.125),
    vec4(0.0, 0.0, 0.0, 0.125),
    vec4(0.0, 2.0, 0.0, 0.125),
    vec4(0.0, -2.0, 0.0, 0.125),
	vec4(1.0, 1.0, 0.0, 0.125),
    vec4(-1.0, -1.0, 0.0, 0.125),
	vec4(-1.0, 1.0, 0.0, 0.125),
    vec4(1.0, -1.0, 0.0, 0.125)
);

// Incoming texture coordinate
layout (location = 0) in vec2 tex_coord;

// Outgoing colour
layout (location = 0) out vec4 colour;

void main()
{
    // Start with colour as black
	 colour = vec4(0.0f , 0.0f , 0.0f , 1.0f);
	vec4 LoopColour = texture(tex ,tex_coord);
	float[8] intensity; 

    // Loop through each sample vector
	vec4  temp = vec4(1.0f , 1.0f , 1.0f , 1.0f);
	int c = 0;
	for (int a = -1; a < 2; ++a)
	{
		for (int b = -1; b < 2; ++b)
		{ 
		    if ( a== 0 && b ==0 )
			{
			 continue;
			}
			LoopColour = texture(tex,tex_coord + vec2(a * invWidth,  b * invHeight ));
			intensity[c] = (LoopColour.r + LoopColour.b  + LoopColour.g)/3;
			c++;
		}
	}
	float avg;
	for(int a = 0 ; a < 4; ++a)
	{
		avg += abs(intensity[a] - intensity[7-a]);
	}	
	avg /= 4.0f;
	avg = clamp(2* avg, 0.0 , 1.0);
	if(avg < 0.1)
	{
		colour = mix(texture(tex,tex_coord), vec4(1,1,1 ,0.3) , 0.1);
	}
	else
	{
	  colour = vec4(0.0 , 0 , 0, 1.0f);
	}
	
    colour.a = 1.0f;

	colour = texture(depthTex, tex_coord);
}