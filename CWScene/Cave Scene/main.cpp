#include <graphics_framework.h>
#include <glm\glm.hpp>
#include <ctime>

using namespace std;
using namespace graphics_framework;
using namespace glm;



// Define struct to hold info on all created/imported items in scene
struct sceneObject
{
	mesh mesh;			// To store geom info

	texture tex;		// Used to store/set texture for this object

	vec3 pos;			// Holds world position of object

};

// Create dictionary to hold all scene objects 
map<string, sceneObject> objects;		

// Effects used
effect pointLightShader;														
effect greyscale;							
effect mask;								
effect contrast;							
effect edge_detection;						
effect depth_detection;						
//effect ambientShader;						// Ambient shaders are now not implemented, as they don't suit the scene.
											// I have included them, commented out, to demonstrate my understanding of them.


// Vars needed for above effects
frame_buffer frameB;						
geometry screen_quad;				
texture alpha_map;							// Texture that is mapped to frame buffer in mask effect
bool gs = false;							// Greyscale toggle
bool maskOn = false;						// Mask toggle
bool edgesDetected = false;					// Edge detection toggle
bool depthDetected = false;					// Depth detection toggle
float contrast_value = 10;					// Sets base contrast value


// Used in ReadHeightMap()
vector<float> heightmap;					

// Cameras for the scene
free_camera fcam;							// Explore scene via free cam
target_camera tcam;							// Shows Points of interest
bool tcamSwitch = false;					// Has user switched to target cam?

// Rotation of scene objects
quat RotationQuat;							// Creates empty quat used for rotation of cave parts


// Vars for setting cursor position, move speed and turn sensitivity
double cursor_x = 0.0;
double cursor_y = 0.0;
float lookSensitivity = 0.005f;				// 0.002 suits player movement feel, debug at 0.005 or more
float camMoveSpeed = 0.2f;

// Lighting
point_light point = point_light();			// Point light that follows free camera

// Time counter
float timeCount = 0.0f;
float timeScale;							// Scale value which uses timeCount




// Takes bmp file, reads data and outputs array of floats used in terrain gen
vector<float> ReadHeightMap(char* filename)
{
	// Create pointer to file
	FILE* f = fopen(filename, "rb");
	// To read header
	unsigned char head[54];
	fread(head, sizeof(unsigned char), 54, f);
	// Set width & height
	int width = *(int*)&head[18];
	int height = *(int*)&head[22];
	// Setup params for reading in data
	int padded_row = (width * 3 + 3) & (~3);
	unsigned char* input = new unsigned char[padded_row];
	// Create empty array 
	vector<float> output;
	// Push data to output array
	for (int i = 0; i < height; i++)
	{
		fread(input, sizeof(unsigned char), padded_row, f);
		for (int j = 0; j < width * 3; j += 3)
		{
			output.push_back((float)input[j] / 255.0f);
		}
	}
	// Close file
	fclose(f);

	return output;
}

// Generates terrain map from given bmp data
geometry Terrain()
{
	// Set to raise/lower entire mesh
	float displaceY = 30.0f;
	// Create array for texture coordinates
	vector<vec2> textureMap;
	// Distance between quads
	float tileSize = 0.5;						// 0.4 produces a slight gap between each quad; useful for viewing geom without much light
	// Positions of vectors to be created
	vector<vec3> terrain;
	float heightMultiplier = 50.0f;
	// Resolution
	int heightmapSize = 2048;
	// Changes mesh stretch
	int sizeMap = 64;
	int sizeMultiplier = heightmapSize / (sizeMap + 1);
	// Read in height map
	heightmap = ReadHeightMap("..\\resources\\textures\\caveceilinghm.bmp");
	
	for (int i = 0; i < sizeMap; i++)
	{
		for (int j = 0; j < sizeMap; j++)
		{
			float x = i - (sizeMap / 2);
			float z = j - (sizeMap / 2);
			float* height = new float[4];
			height[0] = 0.5f * (float)heightmap[(j * sizeMultiplier) + ((i * sizeMultiplier) * heightmapSize)];
			height[1] = 0.5f * (float)heightmap[((j + 1) * sizeMultiplier) + ((i * sizeMultiplier) * heightmapSize)];
			height[2] = 0.5f * (float)heightmap[(j * sizeMultiplier) + (((i + 1) * sizeMultiplier) * heightmapSize)];
			height[3] = 0.5f * (float)heightmap[((j + 1) * sizeMultiplier) + (((i + 1)* sizeMultiplier) * heightmapSize)];

			// Create first triangle of quad
			terrain.push_back(vec3(x + (tileSize), (displaceY + heightMultiplier * -height[2]), z - (tileSize)));
			terrain.push_back(vec3(x - (tileSize), (displaceY + heightMultiplier * -height[1]), z + (tileSize)));
			terrain.push_back(vec3(x - (tileSize), (displaceY + heightMultiplier * -height[0]), z - (tileSize)));

			// Second tri of quad
			terrain.push_back(vec3(x + (tileSize), (displaceY + heightMultiplier * -height[2]), z - (tileSize)));
			terrain.push_back(vec3(x + (tileSize), (displaceY + heightMultiplier * -height[3]), z + (tileSize)));
			terrain.push_back(vec3(x - (tileSize), (displaceY + heightMultiplier * -height[1]), z + (tileSize)));

			// Add texure coords - first tri
			textureMap.push_back(vec2(0.0f, 0.0f));
			textureMap.push_back(vec2(1.0f, 0.0f));
			textureMap.push_back(vec2(1.0f, 1.0f));

			// Second tri tex coords
			textureMap.push_back(vec2(1.0f, 1.0f));
			textureMap.push_back(vec2(0.0f, 1.0f));
			textureMap.push_back(vec2(0.0f, 0.0f));

			// Delete and clear height
			delete height;
			height = NULL;
		}
	}
	// Create geom to hold created terrain
	geometry generatedTerrain;
	// Add array of positions created
	generatedTerrain.add_buffer(terrain, BUFFER_INDEXES::POSITION_BUFFER);
	// Apply texture coords
	generatedTerrain.add_buffer(textureMap, BUFFER_INDEXES::TEXTURE_COORDS_0);
	// Return generated geometry
	return generatedTerrain;
	
}





// Perform on startup
bool initialise()
{

	frameB = frame_buffer(renderer::get_screen_width(), renderer::get_screen_height());

	// Grabs current mouse position, updates to cursor position variables 
	glfwGetCursorPos(renderer::get_window(), &cursor_x, &cursor_y);
	// Disables the cursor
	glfwSetInputMode(renderer::get_window(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Set free cam point light attributes
	point.set_constant_attenuation(1.0f);		// Brightness: <--1 is brighter
	point.set_linear_attenuation(0.005f);		// Light cut off range. Lower the value, farther light travels
	point.set_quadratic_attenuation(0.01f);		// Light degredation over range. High value cuts light off more suddenly
	

	return true;
}

// Perform just after initialise
bool load_content()
{
	// Set up screen quad for use with frame buffer pp effects
	vector<vec3> positions
	{
	vec3(1.0f, 1.0f, 0.0f),
	vec3(-1.0f, 1.0f, 0.0f),
	vec3(-1.0f, -1.0f, 0.0f),
	vec3(1.0f, -1.0f, 0.0f)
};
	vector<vec2> tex_coords
	{
		vec2(1.0f, 1.0f),
		vec2(0.0f, 1.0f),
		vec2(0.0f, 0.0f),
		vec2(1.0f, 0.0f)
	};
	// Add above positions/tex coords to screen quad
	screen_quad.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER);
	screen_quad.add_buffer(tex_coords, BUFFER_INDEXES::TEXTURE_COORDS_0);
	screen_quad.set_type(GL_QUADS);




	// Top ceiling cave part
	// Create empty scene object
	sceneObject caveCeiling;	
	// Assign mesh to newly created geom from height map
	caveCeiling.mesh.set_geometry(Terrain());		
	// Set texture
	caveCeiling.tex = texture("..\\resources\\textures\\cavefloor.png");
	// Add to dictionary
	objects["caveCeiling"] = caveCeiling;			



	// Floor cave part
	// Create empty scene object
	sceneObject caveFloor;
	// Assign mesh to newly created geom from height map
	caveFloor.mesh.set_geometry(Terrain());
	// Set texture
	caveFloor.tex = texture("..\\resources\\textures\\cavefloor.png");
	// Set rotation quat to desired rotation on z axis; flip it 180
	RotationQuat = angleAxis(pi<float>() * -(1.0f), vec3(0, 0, 1));
	// Apply rotation
	caveFloor.mesh.get_transform().rotate(RotationQuat);
	// Set position
	caveFloor.pos = vec3(0.0f, 23.0f, 7.0f);
	// Apply translation of set position
	caveFloor.mesh.get_transform().translate(caveFloor.pos);
	// Add to dictionary
	objects["caveFloor"] = caveFloor;


	// Second floor cave part; behind first
	// Create emtpy scene object
	sceneObject caveFloorBack;	
	// Assign mesh to newly created geom from height map
	caveFloorBack.mesh.set_geometry(Terrain());
	// Set texture
	caveFloorBack.tex = texture("..\\resources\\textures\\cavefloor.png");
	//	Set rotation quat to desired rotation on z axis; flip it 180																																
	RotationQuat = angleAxis(pi<float>() * -(1.0f), vec3(0, 0, 1));		
	// Apply rotation
	caveFloorBack.mesh.get_transform().rotate(RotationQuat);		
	// Set position just behind other floor part
	caveFloorBack.pos = vec3(0.0f, 23.0f, -25.0f);
	// Apply translation of set postion
	caveFloorBack.mesh.get_transform().translate(caveFloorBack.pos);		
	// Add to dictionary
	objects["caveFloorBack"] = caveFloorBack;
	

	// Floor 'paving' 
	// Floor dimensions
	unsigned int width = 82;
	unsigned int depth = 82;
	// Create empty sceneObject to hold floor piece
	sceneObject floor;
	// Set floor mesh to newly created plane
	floor.mesh = mesh(geometry_builder::create_plane(width, depth));
	// Set floor texture
	floor.tex = texture("..\\resources\\textures\\dungeonfloor.jpg");
	// Set floor position
	floor.pos = vec3(0.0f, 0.0f, 0.0f);
	// Apply translation of set position
	floor.mesh.get_transform().translate(floor.pos);
	// Add floor object to object dictionary
	objects["floor"] = floor;


	
	// Pillar dimensions
	unsigned int slices = 10;
	unsigned int stacks = 1;
	vec3 pillarScale(1.0f, 30.0f, 1.0f);
	// Create empty sceneObject to hold pillar
	sceneObject pillar1;
	// Set pillar mesh to newly created cylinder
	pillar1.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	// Set pillar texture
	pillar1.tex = texture("..\\resources\\textures\\pillar.jpg");
	// Set pillar position - center of center row
	pillar1.pos = vec3(0.0f, 15.0f, 0.0f);
	pillar1.mesh.get_transform().translate(pillar1.pos);
	// Add pillar object to dictionary
	objects["pillar1"] = pillar1;
	// Same again for each pillar
	sceneObject pillar2;
	pillar2.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar2.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar2.pos = vec3(0.0f, 15.0f, 20.0f); // Y is half that of scale factor
	pillar2.mesh.get_transform().translate(pillar2.pos);
	objects["pillar2"] = pillar2;
	// Pillar 3
	sceneObject pillar3;
	pillar3.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar3.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar3.pos = vec3(0.0f, 15.0f, 40.0f); 
	pillar3.mesh.get_transform().translate(pillar3.pos);
	objects["pillar3"] = pillar3;
	// Pillar 4
	sceneObject pillar4;
	pillar4.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar4.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar4.pos = vec3(0.0f, 15.0f, -20.0f); 
	pillar4.mesh.get_transform().translate(pillar4.pos);
	objects["pillar4"] = pillar4;
	//Pillar 5
	sceneObject pillar5;
	pillar5.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar5.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar5.pos = vec3(0.0f, 15.0f, -40.0f); 
	pillar5.mesh.get_transform().translate(pillar5.pos);
	objects["pillar5"] = pillar5;


	// Center of second row
	sceneObject pillar6;
	pillar6.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar6.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar6.pos = vec3(20.0f, 15.0f, 0.0f); // Y is half of scale factor, set distance apart via z/x
	pillar6.mesh.get_transform().translate(pillar6.pos);
	objects["pillar6"] = pillar6;
	// Pillar 7
	sceneObject pillar7;
	pillar7.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar7.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar7.pos = vec3(20.0f, 15.0f, 20.0f); // Y is half of scale factor, set distance apart via z/x
	pillar7.mesh.get_transform().translate(pillar7.pos);
	objects["pillar7"] = pillar7;
	// Pillar 8
	sceneObject pillar8;
	pillar8.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar8.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar8.pos = vec3(20.0f, 15.0f, 40.0f); // Y is half of scale factor, set distance apart via z/x
	pillar8.mesh.get_transform().translate(pillar8.pos);
	objects["pillar8"] = pillar8;
	// Pillar 9
	sceneObject pillar9;
	pillar9.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar9.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar9.pos = vec3(20.0f, 15.0f, -20.0f); // Y is half of scale factor, set distance apart via z/x
	pillar9.mesh.get_transform().translate(pillar9.pos);
	objects["pillar9"] = pillar9;
	// Pillar 10
	sceneObject pillar10;
	pillar10.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar10.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar10.pos = vec3(20.0f, 15.0f, -40.0f); // Y is half of scale factor, set distance apart via z/x
	pillar10.mesh.get_transform().translate(pillar10.pos);
	objects["pillar10"] = pillar10;


	// Center of third row
	sceneObject pillar11;
	pillar11.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar11.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar11.pos = vec3(-20.0f, 15.0f, 0.0f); // Y is half of scale factor, set distance apart via z/x
	pillar11.mesh.get_transform().translate(pillar11.pos);
	objects["pillar11"] = pillar11;
	// Pillar 12
	sceneObject pillar12;
	pillar12.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar12.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar12.pos = vec3(-20.0f, 15.0f, 20.0f); // Y is half of scale factor, set distance apart via z/x
	pillar12.mesh.get_transform().translate(pillar12.pos);
	objects["pillar12"] = pillar12;
	// Pillar 13
	sceneObject pillar13;
	pillar13.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar13.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar13.pos = vec3(-20.0f, 15.0f, 40.0f); // Y is half of scale factor, set distance apart via z/x
	pillar13.mesh.get_transform().translate(pillar13.pos);
	objects["pillar13"] = pillar13;
	// Pillar 14
	sceneObject pillar14;
	pillar14.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar14.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar14.pos = vec3(-20.0f, 15.0f, -20.0f); // Y is half of scale factor, set distance apart via z/x
	pillar14.mesh.get_transform().translate(pillar14.pos);
	objects["pillar14"] = pillar14;
	// Pillar 15
	sceneObject pillar15;
	pillar15.mesh = mesh(geometry_builder::create_cylinder(stacks, slices, pillarScale));
	pillar15.tex = texture("..\\resources\\textures\\pillar.jpg");
	pillar15.pos = vec3(-20.0f, 15.0f, -40.0f); // Y is half of scale factor, set distance apart via z/x
	pillar15.mesh.get_transform().translate(pillar15.pos);
	objects["pillar15"] = pillar15;



	// Create empty sceneObject to hold crate
	sceneObject crate;
	// Set crate mesh to newly created box
	crate.mesh = mesh(geometry_builder::create_box());
	// Set texture
	crate.tex = texture("..\\resources\\textures\\crate.jpg");
	// Set position 
	crate.pos = vec3(8.2f, 12.f, 7.0f);
	crate.mesh.get_transform().translate(crate.pos);
	// Add crate object to dictionary
	objects["crate"] = crate;


	// Create emtpy sceneObject to hold tree
	sceneObject tree;
	// Load in tree model
	tree.mesh = mesh(geometry("..\\resources\\models\\tree.obj"));
	// Set texture
	tree.tex = texture("..\\resources\\textures\\bark.jpg");
	// Set position
	tree.pos = vec3(10.0f, 0.0f, 0.0f);
	tree.mesh.get_transform().translate(tree.pos);
	// Add to object dictionary
	objects["tree"] = tree;

	// Load in alpha map for mask pp effect
	alpha_map = texture("..\\resources\\textures\\mask.png");

	// Load ambient light shaders - NEEDS FIDDLING WITH!
//	ambientShader.add_shader("..\\resources\\shaders\\simple_ambient.vert", GL_VERTEX_SHADER);
	//ambientShader.add_shader("..\\resources\\shaders\\simple_ambient.frag", GL_FRAGMENT_SHADER);

	// Load greyscale shaders
	greyscale.add_shader("..\\resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
	greyscale.add_shader("..\\resources\\shaders\\greyscale.frag", GL_FRAGMENT_SHADER);

	// Load spot light shaders
	pointLightShader.add_shader("..\\resources\\shaders\\point.vert", GL_VERTEX_SHADER);
	pointLightShader.add_shader("..\\resources\\shaders\\point.frag", GL_FRAGMENT_SHADER);

	// Load mask shaders
	mask.add_shader("..\\resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
	mask.add_shader("..\\resources\\shaders\\mask.frag", GL_FRAGMENT_SHADER);

	// Load contrast shaders
	contrast.add_shader("..\\resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
	contrast.add_shader("..\\resources\\shaders\\contrast.frag", GL_FRAGMENT_SHADER);

	// Load edge detection shaders
	edge_detection.add_shader("..\\resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
	edge_detection.add_shader("..\\resources\\shaders\\edge_d.frag", GL_FRAGMENT_SHADER);

	// Load depth detection shaders
	depth_detection.add_shader("..\\resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
	depth_detection.add_shader("..\\resources\\shaders\\depth_d.frag", GL_FRAGMENT_SHADER);


	// Build effects
	pointLightShader.build();
	//ambientShader.build();
	greyscale.build();
	mask.build();
	contrast.build();
	edge_detection.build(); 
	depth_detection.build();

	// Set free camera properties
	fcam.set_position(vec3(10.0f, 10.0f, 30.0f));
	fcam.set_target(vec3(-100.0f, 0.0f, -100.0f));
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	fcam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);

	// Set target camera properties
	tcam.set_position(vec3(10.0f, 10.0f, 30.0f));
	auto taspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	tcam.set_projection(quarter_pi<float>(), aspect, 2.414f, 1000.0f);
	// Set to look at tree position
	tcam.set_target(vec3(10.0f, 10.0f, 0.0f));

	
	return true;
}

bool update(float delta_time)
{
	// To store current cursor position
	double currentX;
	double currentY;

	// Get current cursor position
	glfwGetCursorPos(renderer::get_window(), &currentX, &currentY);

	// Create delta variables
	double deltaX;
	double deltaY;

	// Calculate delta values
	deltaX = currentX - cursor_x;
	deltaY = currentY - cursor_y;

	// Update current cursor position
	cursor_x = currentX;
	cursor_y = currentY;


	// Multiply by ratios to lower sensitivity MAKE GLOBAL VAR!!!
	deltaX *= lookSensitivity;
	deltaY *= lookSensitivity;

	// Set camera mouse control inversion (remove - to invert)
	fcam.rotate(deltaX, -deltaY);

	// Emtpy vec3 to store updated cam position
	vec3 camPos(0.0f);

	// Set free camera controls
	// Move forward
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_UP))
		camPos -= camMoveSpeed * (normalize(fcam.get_position() - fcam.get_target()));
	// Move backward
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_DOWN))
		camPos += camMoveSpeed * (normalize(fcam.get_position() - fcam.get_target()));
	// Strafe left
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_LEFT))
		camPos += camMoveSpeed * cross((normalize(fcam.get_position() - fcam.get_target())), vec3(0.0f, 1.0f, 0.0f));
	// Strafe right
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_RIGHT))
		camPos -= camMoveSpeed * cross((normalize(fcam.get_position() - fcam.get_target())), vec3(0.0f, 1.0f, 0.0f));


	// Set target camera control switch
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_T))
		tcamSwitch = true;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_F))
		tcamSwitch = false;

	// Contrast 
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_MINUS))
		contrast_value -= 1;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_EQUAL))
		contrast_value += 1;


	fcam.set_position(fcam.get_position() + camPos);
	// Update spot light position
	point.set_position(fcam.get_position() + camPos);
	// Update the camera
	fcam.update(delta_time);
	// Update target camera
	tcam.update(delta_time);
	

	// Rotate box geom during run time	
	RotationQuat = angleAxis(pi<float>() * -(5.0f / 90.0f), vec3(0, 1, 1));
	objects["crate"].mesh.get_transform().rotate(RotationQuat);
	// Adjust box scale
	timeCount += delta_time;
	timeScale = 1.0f * sinf(timeCount);
	timeScale *= 1.1f;
	// Apply scale adjustments to crate
	objects["crate"].mesh.get_transform().scale = vec3(timeScale, timeScale, timeScale);
	
	return true;
}

bool render()
{
	// Changes background colour from default blue
	glClear(GL_COLOR_BUFFER_BIT);
	glClearColor(1.0f, 0.0f, 0.0f, 1.0f); // black is 1.0 at end only

	// Default free cam properties
	auto V = fcam.get_view();
	auto P = fcam.get_projection();
	// If set to target cam 
	if (tcamSwitch == true)
	{
		V = tcam.get_view();
		P = tcam.get_projection();
	}

	// If any of the pp effects are in use
	if (gs == true || maskOn == true || edgesDetected == true || depthDetected == true)
	{
		// Set render target to frame buffer
		renderer::set_render_target(frameB);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	}

	// Render cave meshes
	for (auto &e : objects)
	{
		auto m = e.second;
		// Bind effect
		renderer::bind(pointLightShader);
		
		// Create MVP matrix
		auto M = m.mesh.get_transform().get_transform_matrix();
		auto MVP = P * V * M;
		// Set MVP matrix uniform
		glUniformMatrix4fv(
			pointLightShader.get_uniform_location("MVP"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(MVP)); // Pointer to matrix data

		// Set M matrix uniform
		glUniformMatrix4fv(pointLightShader.get_uniform_location("M"), 1, GL_FALSE, value_ptr(M));

		// Set N Matrix uniform
		glUniformMatrix3fv(pointLightShader.get_uniform_location("N"), 1, GL_FALSE, value_ptr(m.mesh.get_transform().get_normal_matrix()));

		// Bind material
		renderer::bind(e.second.mesh.get_material(), "mat");
		
		// Bind lights
		renderer::bind(point, "point");
		
		// Bind texture
		renderer::bind(m.tex, 0);
		// Set texture uniform
		glUniform1i(pointLightShader.get_uniform_location("tex"), 0);

		// Set eye position
		glUniform3fv(pointLightShader.get_uniform_location("eye_pos"), 1, value_ptr(fcam.get_position()));


		// Setup ambient light
		//glUniform4fv(eff1.get_uniform_location("material_colour"), 1, value_ptr(vec4(1.0f, 1.0f, 1.0f, 1.0f)));
		//glUniform1f(eff1.get_uniform_location("ambient_intensity"), 0.2f);
		// Setup diffuse light
		//glUniform3fv(eff.get_uniform_location("lightDir"), 1, value_ptr(normalize(cam.get_position() - cam.get_target())));



		// Render mesh
		renderer::render(m.mesh);
	}



	if (gs == true)
	{
		// Set render target back to screen
		renderer::set_render_target();
		// Bind texture shader
		renderer::bind(greyscale);
		// MVP is now identity matrix
		auto MVP = mat4(1.0f);
		glUniformMatrix4fv(
			greyscale.get_uniform_location("MVP"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(MVP)); // Pointer to matrix data

		// Bind texture from frame buffer	
		renderer::bind(frameB.get_frame(), 0);
		// Set the uniform
		glUniform1i(greyscale.get_uniform_location("tex"), 0);
		// Render the screen quad
		renderer::render(screen_quad);
	}
	
	// If masking effect has been toggled
	if (maskOn == true)
	{
		// Set render target back to screen
		renderer::set_render_target();
		// Bind texture shader
		renderer::bind(mask);
		// MVP is now identity matrix
		auto MVP = mat4(1.0f);
		glUniformMatrix4fv(
			mask.get_uniform_location("MVP"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(MVP)); // Pointer to matrix data

		// Bind texture from frame buffer	
		renderer::bind(frameB.get_frame(), 0);
		// Set the uniform
		glUniform1i(mask.get_uniform_location("tex"), 0);
		

		// 	Bind alpha map
		renderer::bind(alpha_map, 1);
		// 
		glUniform1i(mask.get_uniform_location("alpha_map"), 1);

		// Render the screen quad
		renderer::render(screen_quad);
	}

	if (edgesDetected == true)
	{
		// Set render target back to screen
		renderer::set_render_target();
		// Bind texture shader
		renderer::bind(edge_detection);
		// MVP is now identity matrix
		auto MVP = mat4(1.0f);
		glUniformMatrix4fv(
			edge_detection.get_uniform_location("MVP"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(MVP)); // Pointer to matrix data

		// Bind texture from frame buffer	
		renderer::bind(frameB.get_frame(), 0);
		
		// Set the uniform
		glUniform1i(edge_detection.get_uniform_location("tex"), 0);
		
		glUniform1f(edge_detection.get_uniform_location("invWidth"),1.0f / renderer::get_screen_width()); 
		glUniform1f(edge_detection.get_uniform_location("invHeight"), 1.0f / renderer::get_screen_height());
		// Render the screen quad
		renderer::render(screen_quad);
	}

	if (depthDetected == true)
	{
		// Set render target back to screen
		renderer::set_render_target();
		// Bind texture shader
		renderer::bind(depth_detection);
		// MVP is now identity matrix
		auto MVP = mat4(1.0f);
		glUniformMatrix4fv(
			depth_detection.get_uniform_location("MVP"), // Location of uniform
			1, // Number of values - 1 mat4
			GL_FALSE, // Transpose the matrix?
			value_ptr(MVP)); // Pointer to matrix data

		// Bind texture from frame buffer	
		renderer::bind(frameB.get_frame(), 0);
		renderer::bind(frameB.get_depth(), 1);
		// Set the uniform
		glUniform1i(depth_detection.get_uniform_location("tex"), 0);
		glUniform1i(depth_detection.get_uniform_location("depthTex"), 1);
		glUniform1f(depth_detection.get_uniform_location("invWidth"), 1.0f / renderer::get_screen_width());
		glUniform1f(depth_detection.get_uniform_location("invHeight"), 1.0f / renderer::get_screen_height());
		// Render the screen quad
		renderer::render(screen_quad);
	}

	return true;

}

void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods){
	if (action == GLFW_PRESS){
		// Set pp effect controls
		// Greyscale
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_G))
			gs = !gs;
		// Mask
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_M))
			maskOn = !maskOn;
		// Edge Detection
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_E))
			edgesDetected = !edgesDetected;
		// Depth Detection
		if (glfwGetKey(renderer::get_window(), GLFW_KEY_D))
			depthDetected = !depthDetected;
	}
}


void main()
{
	// Create application
	app application;
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_update(update);
	application.set_render(render);

	glfwSetKeyCallback(renderer::get_window(), keyCallback);

	// Get initial cursor position
	initialise();

	// Run application
	application.run();

}